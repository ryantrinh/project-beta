from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder
from datetime import date, time


# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin"
    ]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_id",
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer",
        "date",
        "time",
        "technician",
        "reason",
        "completed",
        "cancelled",
    ]
    def default(self, obj):
        if isinstance(obj, AutomobileVO):
            return obj.vin
        if isinstance(obj, date):
            return obj.strftime("%Y-%m-%d")
        if isinstance(obj, time):
            return obj.strftime("%H:%M")
        if isinstance(obj, Technician):
            return obj.name
        return super().default(obj)
    def get_api_url(self, obj):
        return obj.get_api_url()


class CreateAppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer",
        "date",
        "time",
        "technician",
        "reason",
    ]
    def get_extra_data(self, obj):
        return {
            "vin": obj.vin.vin,
            "technician": obj.technician.name
                }


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder = TechnicianDetailEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create the technician"},
                status=400
            )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse (
                {"message": "Technician does not exist"},
                status=400
            )

    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(employee_id=content["technician"])
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400
            )
        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            print(content)
            vin = AutomobileVO.objects.get(vin=content["vin"])
            print(vin)
            content["vin"] = vin
            technician = Technician.objects.get(name=content["technician"])
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=CreateAppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(pk=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400
            )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if "appointment" in content:
                appointment = Appointment.objects.get(pk=content["appointment"])
                content["appointment"] = appointment
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400
            )
        Appointment.objects.filter(pk=pk).update(**content)
        appointment = Appointment.objects.get(pk=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )

@require_http_methods(["PUT"])
def api_appointment_cancelled(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.cancelled = True
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    except Exception as e:
        return JsonResponse(
            {"message": f"Could not create appointment- {e}"},
            status=400
        )

@require_http_methods(["PUT"])
def api_appointment_completed(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.completed = True
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    except Exception as e:
        return JsonResponse(
            {"message": f"Could not create appointment- {e}"},
            status=400
        )
