from django.db import models
from django.urls import reverse

# Create your models here.

class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)
    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    def __str__(self):
        return self.vin

class Appointment(models.Model):
    customer = models.CharField(max_length=200)
    reason = models.CharField(max_length=300)
    date = models.DateField(default=None)
    time = models.TimeField(default=None)
    completed = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    vin = models.ForeignKey(
        AutomobileVO,
        related_name="vinVO",
        on_delete=models.CASCADE,
    )
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )
    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
    def __str__(self):
        return self.vin
