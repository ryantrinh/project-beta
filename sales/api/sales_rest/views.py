from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Salesperson, Sale, Customer, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "salesperson",
        "customer",
        "id",
    ]
    encoders = {
        "automobile":AutomobileVOEncoder(),
        "salesperson":SalespersonEncoder(),
        "customer":CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def salespeople_list(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
            salesperson,
            encoder= SalespersonEncoder,
            safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "could not create salesperson"},
            )
            response.status_code=400
            return response

@require_http_methods(["DELETE"])
def delete_salesperson(request, pk):
    if request.method == "DELETE":
        try:
            model = Salesperson.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["GET", "POST"])
def customer_list(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
            customer,
            encoder= CustomerEncoder,
            safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "could not create customer"},
            )
            response.status_code=400
            return response

@require_http_methods(["DELETE"])
def delete_customer(request, pk):
    if request.method == "DELETE":
        try:
            model = Customer.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["GET", "POST"])
def sale_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin, sold=False)
            content["automobile"] = automobile
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(pk=salesperson_id)
            content["salesperson"] = salesperson
            customer_id = content["customer"]
            customer = Customer.objects.get(pk=customer_id)
            content["customer"] = customer

            automobile.sold = True
            automobile.save()

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder= SaleEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "could not create sale"},
            )
            response.status_code=400
            return response

@require_http_methods(["DELETE"])
def delete_sale(request, pk):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
