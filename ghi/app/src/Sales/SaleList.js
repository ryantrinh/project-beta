import React, { useEffect, useState } from 'react';

function SaleList(sale) {
    const [saleList, setSaleList] = useState([])
    async function loadSale() {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok){
            const data = await response.json();
            setSaleList(data.sales)
        }
        else{
            console.error(response);
        }
    }
    useEffect (() => {
        loadSale()
    }, []);

    return (
        <div>
        <h1>Sales</h1>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer Name</th>
            <th>Vin</th>
            <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {saleList.map(sale => {
                return (
                <tr key={sale.id}>
                <td>{ sale.salesperson.employee_id}</td>
                <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                <td>{ sale.automobile.vin }</td>
                <td>${ sale.price }.00</td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>
    );
}

export default SaleList
