import React, { useEffect, useState } from 'react';

function SalesHistory () {
    const [salesHistory, setSalesHistory] = useState([]);
    const [salesPerson, setSalesperson] = useState('');

    const handleSalesperson = event => {
        const value = event.target.value;
            setSalesperson(value)
      }

    const fetchSalesHistory = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok){
        const data = await response.json();
        setSalesHistory(data.sales)
    }
}

    useEffect (() => {
        fetchSalesHistory();
  }, []);

        return (
            <div>
            <h1>Salesperson History</h1>
            <select onChange={handleSalesperson} placeholder="Salesperson" required type="text" id="salesperson" name="salesperson" className="form-control" value={salesPerson}>
                <option value=''>Choose a Salesperson</option>
                    {salesHistory.map(sale => {
                        return (
                            <option key={sale.salesperson.id} value={sale.salesperson.first_name}>
                                {sale.salesperson.first_name} {sale.salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
            <table className="table table-striped">
            <thead>
                <tr>
                <th>Salesperson Name</th>
                <th>Customer Name</th>
                <th>Vin</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {salesHistory.filter((sale) => sale.salesperson.first_name === salesPerson).map((sale) => {
                    return (
                    <tr key={sale.id}>
                    <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                    <td>{ sale.automobile.vin }</td>
                    <td>${ sale.price }.00</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </div>
        );
    }

export default SalesHistory;
