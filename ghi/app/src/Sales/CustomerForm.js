import React, { useEffect, useState } from 'react';

function CustomerForm (props) {
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headsers:{
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        }
    }
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    const handleFirstName = (event) => {
        const value = event.target.value;
            setFirstName(value);
    }
    const handleLastName = (event) => {
        const value = event.target.value;
            setLastName(value);
    }
    const handleAddress = (event) => {
        const value = event.target.value;
            setAddress(value);
    }
    const handlePhoneNumber = (event) => {
        const value = event.target.value;
            setPhoneNumber(value);
    }

    useEffect (() => {

    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstName} placeholder="First Name" required type="text" id="first_name" name="first_name" className="form-control" value={firstName}/>
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastName}placeholder="Last Name" required type="text" id="last_name" name="last_name" className="form-control" value={lastName}/>
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAddress} placeholder="Address" required type="text" id="address" name="address" className="form-control" value={address}/>
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePhoneNumber} placeholder="Phone Number" required type="text" id="phone_number" name="phone_number" className="form-control" value={phoneNumber}/>
                <label htmlFor="phone_number">Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default CustomerForm;
