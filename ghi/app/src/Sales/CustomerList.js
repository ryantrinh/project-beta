import React, { useEffect, useState } from 'react';

function CustomerList(customer) {
    const [customerList, setCustomerList] = useState([])
    async function loadCustomer() {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (response.ok){
            const data = await response.json();
            setCustomerList(data.customer)
        }
        else{
            console.error(response);
        }
    }
    useEffect (() => {
        loadCustomer()
    }, []);

    return (
        <div>
        <h1>Customers</h1>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Frist Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
            </tr>
        </thead>
        <tbody>
            {customerList.map(customer => {
                return (
                <tr key={customer.id}>
                <td>{ customer.first_name }</td>
                <td>{ customer.last_name }</td>
                <td>{ customer.phone_number }</td>
                <td>{ customer.address }</td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>
    );
}

export default CustomerList
