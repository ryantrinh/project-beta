import React, { useEffect, useState } from 'react';

function ManufacturerList(manufacturer) {
    const [manufacturerList, setManufacturerList] = useState([])
    async function loadManufacturer(){
        const response = await fetch("http://localhost:8100/api/manufacturers/");
        if (response.ok) {
            const data = await response.json();
            setManufacturerList(data.manufacturers)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadManufacturer()
    }, []);

    return (
        <div>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Name</th>
            </tr>
        </thead>
        <tbody>
            { manufacturerList.map(manufacturer => {
                return (
                <tr key={manufacturer.id}>
                <td>{ manufacturer.name }</td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>
    )
}

export default ManufacturerList
