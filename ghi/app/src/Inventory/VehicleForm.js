import React, { useEffect, useState } from "react";

export default function VehicleForm(props) {
    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [name, setName] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;
        const vehicleUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        
        const response = await fetch(vehicleUrl, fetchConfig);
        if (response.ok) {
            const newVehicle = await response.json();
            props.onVehicleModelCreated(newVehicle);
            setName("");
            setPictureUrl("");
            setManufacturer("");
        }
    }
    const fetchData = async () => {
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerUrl);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }
    useEffect(() => {
        fetchData();
    } , []);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create A Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-vehicle-form">
                    <div className="form-floating mb-3">
                        <input
                        value={name}
                        onChange={handleNameChange}
                        placeholder="Name"
                        required type="text"
                        name="name"
                        id="name"
                        className="form-control"
                        />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                        value={pictureUrl}
                        onChange={handlePictureUrlChange}
                        placeholder="Picture URL"
                        required type="url"
                        name="pictureUrl"
                        id="pictureUrl"
                        className="form-control"
                        />
                        <label htmlFor="modelName">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select
                        value={manufacturer}
                        onChange={handleManufacturerChange}
                        required
                        id="manufacturer"
                        name="manufacturer"
                        className="form-select"
                        >
                        <option key="default" value="">Choose A Manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                            )}
                        )}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
