import React, { useEffect, useState } from "react";

export default function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        getTechnician();
    } , []);
    function getTechnician() {
        fetchData();
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <h1>Technicians</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Employee Id</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {technicians.map((technician, id) => (
                            <tr key={id} value={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.name}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
