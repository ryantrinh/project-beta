import React, {useEffect, useState } from 'react';


export default function AppointmentForm() {
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date = date;
        data.time = time;
        data.technician = technician;
        data.reason = reason;

        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const appointment = await response.json();
            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setTechnician('');
            setReason('');
        }
    };

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    };

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    };

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    };

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    };

    useEffect(() => {
        fetchData();
    } , []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create A New Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input
                            value={vin}
                            onChange={handleVinChange}
                            placeholder="Vin"
                            required type="text"
                            name="vin"
                            id="vin"
                            className="form-control"
                            />
                            <label htmlFor="vin">Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={customer}
                            onChange={handleCustomerChange}
                            placeholder="Customer"
                            required type="text"
                            name="customer"
                            id="customer"
                            className="form-control"
                            />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={date}
                            onChange={handleDateChange}
                            placeholder="Date"
                            required type="date"
                            name="date" id="date"
                            className="form-control"
                            />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                            value={time}
                            onChange={handleTimeChange}
                            placeholder="Time"
                            required type="time"
                            name="time"
                            id="time"
                            className="form-control"
                            />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select
                            value={technician}
                            onChange={handleTechnicianChange}
                            required
                            id="technician"
                            name="technician"
                            className="form-select"
                            >
                            <option value="">Choose a technican</option>
                            {technicians.map((technician, id) => {
                                return (
                                    <option
                                    key={id}
                                    value={technician.id}
                                    >
                                        {technician.name}
                                    </option>
                                );}
                            )}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="reason">Reason</label>
                            <textarea
                            value={reason}
                            onChange={handleReasonChange}
                            required type="text"
                            name="reason"
                            id="reason"
                            rows="3"
                            className="form-control"
                            ></textarea>
                        </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
