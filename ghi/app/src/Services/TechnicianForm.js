import React, { useState } from 'react';


export default function TechnicianForm() {
    const [name, setName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.employee_id = employeeId;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            setName('');
            setEmployeeId('');
        }
    };

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    };




    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                    <div className="form-floating mb-3">
                        <input
                        value={name}
                        onChange={handleNameChange}
                        placeholder="Name"
                        required type="text"
                        name="name"
                        id="name"
                        className="form-control"
                        />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                        value={employeeId}
                        onChange={handleEmployeeIdChange}
                        placeholder="Employee ID"
                        required type="text"
                        name="employeeId"
                        id="employeeId"
                        className="form-control"
                        />
                        <label htmlFor="employeeId">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
