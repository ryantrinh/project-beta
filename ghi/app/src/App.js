import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useEffect, useState } from "react";

import MainPage from './MainPage';
import Nav from './Nav';

// Sale
import CustomerList from './Sales/CustomerList';
import CustomerForm from './Sales/CustomerForm';
import SalespersonList from './Sales/SalespersonList';
import SalespersonForm from './Sales/SalespersonForm';
import SaleList from './Sales/SaleList';
import SaleForm from './Sales/SaleForm';
import SalesHistory from './Sales/SalespersonHistory';

// Services
import TechnicianForm from "./Services/TechnicianForm";
import TechnicianList from './Services/TechnicianList';
import AppointmentForm from './Services/AppointmentForm';
import AppointmentList from './Services/AppointmentList';
import AppointmentHistory from './Services/AppointmentHistory';

// Inventory
import AutomobileForm from './Inventory/AutomobileForm';
import VehicleForm from './Inventory/VehicleForm';
import AutomobileList from './Inventory/AutomobileList';
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelList from './Inventory/ModelList'


export default function App() {
  const [automobiles, setAutomobiles] = useState([]);
  const [vehicleModel, setVehicleModel] = useState([]);

  const fetchAutomobilesData = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  const fetchVehicleModelData = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setVehicleModel(data.models);
    }
  };

  const handleAutomobileCreated = (newAutomobile) => {
    setAutomobiles((oldAutomobiles) => [...oldAutomobiles, newAutomobile]);
  };

  const handleVehicleModelCreated = (newVehicleModel) => {
    setVehicleModel((oldVehicleModels) => [
      ...oldVehicleModels,
      newVehicleModel,
    ]);
  };

  useEffect(() => {
    fetchAutomobilesData();
    fetchVehicleModelData();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
            <Route path="" element={<TechnicianList />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm />} />
            <Route path="" element={<AppointmentList />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="models">
              <Route
                path="new"
                element={
                  <VehicleForm
                    onVehicleModelCreated={handleVehicleModelCreated}
                  />
                }
              />
              <Route path="" element={<ModelList />} />
            </Route>

          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
          <Route
            path="new"
            element={
              <AutomobileForm
                onAutomobileCreated={handleAutomobileCreated}
              />
            }
          />
          </Route>
          <Route path="customer">
            <Route path="new" element={<CustomerForm />} />
            <Route index element={<CustomerList />} />
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalespersonForm />} />
            <Route index element={<SalespersonList />} />
          </Route>
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
            <Route index element={<ManufacturerList />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelList />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SaleForm />} />
            <Route path="history" element={<SalesHistory />} />
            <Route index element={<SaleList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
